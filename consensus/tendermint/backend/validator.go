package backend

import (
	"math/big"

	"gitlab.com/lightnet1/evrynet-node/consensus"
	"gitlab.com/lightnet1/evrynet-node/consensus/tendermint"
)

//ValidatorSetInfo keep tracks of validator set in associate with blockNumber
type ValidatorSetInfo interface {
	GetValSet(chainReader consensus.ChainReader, blockNumber *big.Int) (tendermint.ValidatorSet, error)
}
